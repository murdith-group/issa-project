import os
import sentry_sdk
version = os.environ.get("CI_COMMIT_SHA", "1.0.0")
# project doom
sentry_sdk.init(dsn='https://4c0b4d78a85e4531a128a4730cd4c70b@o49697.ingest.sentry.io/4511291', release=version)

def simple():
    try: 
        try_again()
    except Exception as err:
        sentry_sdk.capture_exception(err)

simple()